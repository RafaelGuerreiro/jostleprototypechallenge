package com.jostle.prototype.challenge.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface PhotosServiceAsync {
	void loadResource(String url, AsyncCallback<String> callback);
}
