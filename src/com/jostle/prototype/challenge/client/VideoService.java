package com.jostle.prototype.challenge.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("video")
public interface VideoService extends RemoteService {
	String loadResource(String url);
}
