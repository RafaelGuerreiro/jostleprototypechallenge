package com.jostle.prototype.challenge.client.converter;

public interface Converter<T> {
	T convert(String value);
}
