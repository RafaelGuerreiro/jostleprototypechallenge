package com.jostle.prototype.challenge.client.converter.implementation;

import static java.lang.Integer.valueOf;

import com.jostle.prototype.challenge.client.converter.Converter;

public class IntegerConverter implements Converter<Integer> {

	@Override
	public Integer convert(String value) {
		if (value == null)
			return null;

		return valueOf(value);
	}
}
