package com.jostle.prototype.challenge.client.converter.implementation;

import static com.jostle.prototype.challenge.client.animation.Animation.valueOf;

import com.jostle.prototype.challenge.client.animation.Animation;
import com.jostle.prototype.challenge.client.converter.Converter;

public class AnimationConverter implements Converter<Animation> {

	@Override
	public Animation convert(String value) {
		if (value == null)
			return null;

		return valueOf(value);
	}
}
