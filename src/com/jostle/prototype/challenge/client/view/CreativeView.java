package com.jostle.prototype.challenge.client.view;

import static com.jostle.prototype.challenge.client.repository.RepositoryKey.CREATIVE_VIEW_TITLE;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.PHOTOS_ANIMATION;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.PHOTOS_PER_PAGE;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.PHOTOS_URL;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.PHOTOS_VIEW_TITLE;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.TRANSITION_ANIMATION;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.VIDEO_RESOURCE_URL;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.VIDEO_URL;
import static com.jostle.prototype.challenge.client.repository.RepositoryKey.VIDEO_VIEW_TITLE;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.jostle.prototype.challenge.client.animation.Animation;
import com.jostle.prototype.challenge.client.repository.ClientRepository;
import com.jostle.prototype.challenge.client.repository.RepositoryKey;
import com.jostle.prototype.challenge.client.view.validator.HttpUrlValidator;
import com.jostle.prototype.challenge.client.view.validator.LengthValidator;
import com.jostle.prototype.challenge.client.view.widget.IntegerValidableTextBox;
import com.jostle.prototype.challenge.client.view.widget.SelectableListBox;
import com.jostle.prototype.challenge.client.view.widget.ValidableTextBox;

public class CreativeView extends Composite implements View {
	private static ThirdViewUiBinder uiBinder = GWT.create(ThirdViewUiBinder.class);

	private final ClientRepository repository;

	interface ThirdViewUiBinder extends UiBinder<Widget, CreativeView> {
	}

	@UiField
	SelectableListBox transitionAnimation;

	@UiField
	ValidableTextBox videoViewTitle;

	@UiField
	ValidableTextBox videoURL;

	@UiField
	ValidableTextBox videoResourceURL;

	@UiField
	ValidableTextBox photosViewTitle;

	@UiField
	IntegerValidableTextBox photosPerPage;

	@UiField
	ValidableTextBox photosURL;

	@UiField
	SelectableListBox photosAnimation;

	@UiField
	ValidableTextBox creativeViewTitle;

	@UiField
	Anchor changeVideoResource;

	@UiField
	Anchor changePhotosResource;

	public CreativeView(ClientRepository repository) {
		initWidget(uiBinder.createAndBindUi(this));
		this.repository = repository;

		setWidgets();
	}

	private void setWidgets() {
		addAnimationsInto(transitionAnimation);
		addAnimationsInto(photosAnimation);

		addChangeHandlersForUpdatingTheRepository();

		setAsNumericInput();

		setAsTitle(videoViewTitle);
		setAsTitle(photosViewTitle);
		setAsTitle(creativeViewTitle);

		setAsHttpUrl(videoURL);
		setAsHttpUrl(videoResourceURL);
		setAsHttpUrl(photosURL);

		switchValueOfInput(changeVideoResource, videoResourceURL);
		switchValueOfInput(changePhotosResource, photosURL);
	}

	private void switchValueOfInput(final Anchor anchor, final ValidableTextBox textbox) {
		anchor.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String oldValue = textbox.getValue();
				String newValue = anchor.getHref();

				textbox.setValue(newValue);
				textbox.fireEvent(new ChangeEvent() {
				});

				anchor.setHref(oldValue);
				anchor.setTitle(oldValue);

				event.preventDefault();
				event.stopPropagation();
			}
		});
	}

	private void setAsHttpUrl(ValidableTextBox text) {
		text.addValidator(new HttpUrlValidator());
	}

	private void setAsNumericInput() {
		photosPerPage.initialize().setLimitBoundaries(2, 15)
				.addIntegerValidatorWithMessage("Photos per page should be between 2 and 15.");
	}

	private void setAsTitle(ValidableTextBox text) {
		text.setMaxLength(15);
		text.addValidator(new LengthValidator("The title should have between 3 and 15 characters.", 3, 15));
	}

	private void addChangeHandlersForUpdatingTheRepository() {
		addChangeHandler(transitionAnimation, TRANSITION_ANIMATION);
		addChangeHandler(videoViewTitle, VIDEO_VIEW_TITLE);
		addChangeHandler(videoURL, VIDEO_URL);
		addChangeHandler(videoResourceURL, VIDEO_RESOURCE_URL);
		addChangeHandler(photosViewTitle, PHOTOS_VIEW_TITLE);
		addChangeHandler(photosPerPage, PHOTOS_PER_PAGE);
		addChangeHandler(photosURL, PHOTOS_URL);
		addChangeHandler(photosAnimation, PHOTOS_ANIMATION);
		addChangeHandler(creativeViewTitle, CREATIVE_VIEW_TITLE);
	}

	private void addAnimationsInto(ListBox list) {
		for (Animation animation : Animation.values())
			list.addItem(animation.getDisplay(), animation.name());
	}

	private void addChangeHandler(final ValidableTextBox text, final RepositoryKey key) {
		text.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (text.validate())
					repository.set(key, text.getValue());
			}
		});
	}

	private void addChangeHandler(final ListBox list, final RepositoryKey key) {
		list.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				repository.set(key, list.getSelectedValue());
			}
		});
	}

	@Override
	public void loadBackendData() {
	}

	@Override
	public void onDisplay() {
		updateInputValues();
	}

	private void updateInputValues() {
		selectValue(transitionAnimation, repository.getTransitionAnimation());
		selectValue(photosAnimation, repository.getPhotosAnimation());

		setValue(videoViewTitle, repository.getVideoViewTitle());
		setValue(videoURL, repository.getVideoURL());
		setValue(videoResourceURL, repository.getVideoResourceURL());
		setValue(photosViewTitle, repository.getPhotosViewTitle());
		setValue(photosPerPage, repository.getPhotosPerPage());
		setValue(photosURL, repository.getPhotosURL());
		setValue(creativeViewTitle, repository.getCreativeViewTitle());
	}

	private void setValue(ValidableTextBox text, Object value) {
		if (value == null)
			value = "";

		text.setText(value.toString());
		text.validate();
	}

	private void selectValue(SelectableListBox list, Animation animation) {
		list.setSelectedValue(animation.name());
	}

	@Override
	public String getPageTitle() {
		return repository.getCreativeViewTitle();
	}
}