package com.jostle.prototype.challenge.client.view;

import com.google.gwt.user.client.ui.Composite;

public abstract class CompositeView extends Composite implements View {
	@Override
	public final void loadBackendData() {
		if (!loadBackendDataWhen())
			return;

		loadResource();
	}

	protected abstract void loadResource();

	protected boolean loadBackendDataWhen() {
		boolean always = true;
		return always;
	}
}
