package com.jostle.prototype.challenge.client.view.validator;

public interface Validator {
	String getMessage();
	
	boolean validate(String value);
}
