package com.jostle.prototype.challenge.client.view.validator;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;
import static java.lang.Integer.parseInt;

public class IntegerValidator implements Validator {

	private final String message;
	private final int min;
	private final int max;

	public IntegerValidator() {
		this("Should be an integer.", null, null);
	}

	public IntegerValidator(String message) {
		this(message, null, null);
	}

	public IntegerValidator(String message, Integer min, Integer max) {
		this.message = message;
		this.min = min == null ? MIN_VALUE : min;
		this.max = max == null ? MAX_VALUE : max;
	}

	@Override
	public boolean validate(String value) {
		if (!value.matches("\\d+"))
			return false;

		int number = parseInt(value);
		return number >= min && number <= max;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
