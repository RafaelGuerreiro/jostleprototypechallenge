package com.jostle.prototype.challenge.client.view.validator;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Integer.MIN_VALUE;

public class LengthValidator implements Validator {

	private final String message;
	private final int min;
	private final int max;

	public LengthValidator(String message, Integer min, Integer max) {
		this.message = message;
		this.min = min == null ? MIN_VALUE : min;
		this.max = max == null ? MAX_VALUE : max;
	}

	@Override
	public boolean validate(String value) {
		int length = value.length();
		return length >= min && length <= max;
	}

	@Override
	public String getMessage() {
		return message;
	}
}
