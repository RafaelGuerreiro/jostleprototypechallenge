package com.jostle.prototype.challenge.client.view.validator;

import com.google.gwt.regexp.shared.RegExp;

public class HttpUrlValidator implements Validator {

	private final static RegExp URL_PATTERN = RegExp.compile("^http(s)?:\\/\\/([^?#]*)(\\#.*)?$", "i");

	private final String message;

	public HttpUrlValidator() {
		this("This is not a valid URL using HTTP/HTTPS protocol.");
	}

	public HttpUrlValidator(String message) {
		this.message = message;
	}

	@Override
	public boolean validate(String value) {
		return URL_PATTERN.test(value);
	}

	@Override
	public String getMessage() {
		return message;
	}
}
