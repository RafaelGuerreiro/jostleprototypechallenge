package com.jostle.prototype.challenge.client.view;

public interface View {
	void onDisplay();
	
	void loadBackendData();

	String getPageTitle();
}
