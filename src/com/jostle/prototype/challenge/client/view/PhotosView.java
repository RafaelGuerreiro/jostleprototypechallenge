package com.jostle.prototype.challenge.client.view;

import static java.lang.String.valueOf;
import static java.math.RoundingMode.UP;

import java.math.BigDecimal;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.jostle.prototype.challenge.client.PhotosService;
import com.jostle.prototype.challenge.client.PhotosServiceAsync;
import com.jostle.prototype.challenge.client.json.PhotoJavaScriptObject;
import com.jostle.prototype.challenge.client.repository.ClientRepository;
import com.jostle.prototype.challenge.client.view.pagination.FixedPageCalculator;
import com.jostle.prototype.challenge.client.view.pagination.FixedRatePageCalculator;
import com.jostle.prototype.challenge.client.view.pagination.NoopPageCalculator;
import com.jostle.prototype.challenge.client.view.pagination.PageCalculator;
import com.jostle.prototype.challenge.client.view.widget.PhotoWidget;

public class PhotosView extends CompositeView {
	private static SecondViewUiBinder uiBinder = GWT.create(SecondViewUiBinder.class);

	interface SecondViewUiBinder extends UiBinder<Widget, PhotosView> {
	}

	private final PhotosServiceAsync service = GWT.create(PhotosService.class);

	private final ClientRepository repository;

	private int photosPerPage;

	private boolean photosPerPageUpdated = true;

	private String lastRequestedUrl;

	private JsArray<PhotoJavaScriptObject> photos;

	@UiField
	ListBox switchToPage;

	@UiField
	SpanElement lastPage;

	@UiField
	FlowPanel photosContainer;

	@UiField
	Button first;

	@UiField
	Button previous;

	@UiField
	Button next;

	@UiField
	Button last;

	private int currentPage = 0;

	public PhotosView(ClientRepository repository) {
		this.repository = repository;
		initWidget(uiBinder.createAndBindUi(this));

		attachClickHandlerTo(first, new FixedPageCalculator(0));
		attachClickHandlerTo(previous, new FixedRatePageCalculator(-1));
		attachClickHandlerTo(next, new FixedRatePageCalculator(1));
		attachClickHandlerTo(last, goToLastPage());

		switchToPage.addChangeHandler(buildChangeHandler());
	}

	private void attachClickHandlerTo(Button button, PageCalculator calculator) {
		button.addClickHandler(buildClickHandler(calculator));
	}

	private ClickHandler buildClickHandler(final PageCalculator calculator) {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				updatePageUsing(calculator);
			}
		};
	}

	private ChangeHandler buildChangeHandler() {
		return new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				PageCalculator pageCalculator = new FixedPageCalculator(switchToPage.getSelectedValue());
				updatePageUsing(pageCalculator);
			}
		};
	}

	private void updatePageUsing(PageCalculator pageCalculator) {
		if (pageCalculator == null)
			return;

		updatePageToPage(pageCalculator.getNextPage(currentPage));
	}

	private void updatePage() {
		updatePageUsing(new NoopPageCalculator());
	}

	private void updatePageToPage(int page) {
		if (photos == null)
			return;

		currentPage = page;

		switchToPage.setSelectedIndex(currentPage);

		addWidgets();
		updateNavigationButtons();

		photosPerPageUpdated = false;
	}

	@Override
	public void onDisplay() {
		if (photos == null)
			return;

		updatePhotosPerPage();
		updatePagesInformation();
	}

	private void updatePagesInformation() {
		setPagesIntoSelectBox();
		lastPage.setInnerText(valueOf(getDisplayMaximumPages()));
	}

	private int getPhotosPerPage() {
		if (photosPerPageUpdated || photosPerPage < 1)
			return repository.getPhotosPerPage();

		return photosPerPage;
	}

	private void updatePhotosPerPage() {
		if (photosPerPageUpdated = (photosPerPage != repository.getPhotosPerPage()))
			photosPerPage = repository.getPhotosPerPage();
	}

	private void setPagesIntoSelectBox() {
		if (!photosPerPageUpdated)
			return;

		switchToPage.clear();

		for (int page = 0; page < getMaximumPages(); page++)
			switchToPage.addItem(valueOf(page + 1), valueOf(page));
	}

	private void addWidgets() {
		photosContainer.clear();

		for (int index = getMinimumIndex(); index <= getMaximumIndex(); index++)
			photosContainer.add(createWidget(index));
	}

	private void updateNavigationButtons() {
		previous.setEnabled(currentPage > 0);
		next.setEnabled(currentPage < getMaximumPages() - 1);
	}

	private Widget createWidget(int index) {
		return new PhotoWidget(index, photos.get(index), repository);
	}

	private int getMinimumIndex() {
		return currentPage * getPhotosPerPage();
	}

	private int getMaximumIndex() {
		int maximumIndex = getMinimumIndex() + getPhotosPerPage() - 1;
		return maximumIndex >= photos.length() ? photos.length() - 1 : maximumIndex;
	}

	private int getMaximumPages() {
		if (photos == null)
			return 0;

		BigDecimal pages = new BigDecimal(photos.length()).divide(new BigDecimal(getPhotosPerPage()), 0, UP);
		return pages.intValue();
	}

	private int getDisplayMaximumPages() {
		return getMaximumPages();
	}

	@Override
	protected boolean loadBackendDataWhen() {
		return photos == null || lastRequestedUrl != repository.getPhotosURL();
	}

	@Override
	protected void loadResource() {
		lastRequestedUrl = repository.getPhotosURL();
		service.loadResource(lastRequestedUrl, getServiceCallback());
	}

	private AsyncCallback<String> getServiceCallback() {
		return new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				setPhotos(JsonUtils.<JsArray<PhotoJavaScriptObject>> safeEval(result));

				updatePagesInformation();
				updatePage();
			}

			@Override
			public void onFailure(Throwable caught) {
				throw new RuntimeException(caught);
			}
		};
	}

	private void setPhotos(JsArray<PhotoJavaScriptObject> photos) {
		if (photos != null && photos.length() > 0)
			this.photos = photos;
	}

	@Override
	public String getPageTitle() {
		return repository.getPhotosViewTitle();
	}

	private PageCalculator goToLastPage() {
		return new PageCalculator() {
			@Override
			public int getNextPage(int actualPage) {
				int limit = getMaximumPages();

				if (limit == 0)
					return limit;

				return limit - 1;
			}
		};
	}
}
