package com.jostle.prototype.challenge.client.view.widget;

import com.jostle.prototype.challenge.client.view.validator.IntegerValidator;

public class IntegerValidableTextBox extends ValidableTextBox {

	private Integer min;
	private Integer max;

	public IntegerValidableTextBox initialize() {
		getElement().setAttribute("type", "number");
		return this;
	}

	public IntegerValidableTextBox setLimitBoundaries(Integer min, Integer max) {
		return setMin(min).setMax(max);
	}

	private IntegerValidableTextBox setMin(Integer min) {
		this.min = min;
		setAttribute("min", min);

		return this;
	}

	private IntegerValidableTextBox setMax(Integer max) {
		this.max = max;
		setAttribute("max", max);

		if (max != null)
			setMaxLength(max.toString().length());

		return this;
	}

	public IntegerValidableTextBox addIntegerValidatorWithMessage(String message) {
		addValidator(new IntegerValidator(message, min, max));
		return this;
	}
}
