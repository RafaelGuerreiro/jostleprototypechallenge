package com.jostle.prototype.challenge.client.view.widget;

import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.jostle.prototype.challenge.client.json.PhotoJavaScriptObject;
import com.jostle.prototype.challenge.client.repository.ClientRepository;

public class PhotoWidget extends Composite {

	private final ClientRepository repository;
	private final PhotoJavaScriptObject photo;
	private final int index;

	private final FocusPanel wrapper;
	private final FlowPanel panel;

	private final Image image = new Image();
	private final HTMLPanel title;
	private final HTMLPanel indexIndicator;
	private final ModalWidget modal;

	public PhotoWidget(int index, PhotoJavaScriptObject photo, ClientRepository repository) {
		if (photo == null)
			throw new IllegalArgumentException("The photo cannot be null.");

		this.index = index;
		this.photo = photo;
		this.repository = repository;

		setImage();

		this.indexIndicator = new HTMLPanel(SpanElement.TAG, buildIndexIndicator());
		setIndexIndicator(indexIndicator);

		this.title = new HTMLPanel(HeadingElement.TAG_H3, "");
		setTitle(title);

		modal = new ModalWidget("Photo " + buildIndexIndicator());
		setModal(modal);

		wrapper = new FocusPanel();
		panel = new FlowPanel();
		setPanel(wrapper, panel);

		initWidget(wrapper);
	}

	private void setModal(ModalWidget modal) {
		HTMLPanel paragraph = new HTMLPanel(ParagraphElement.TAG, getTitle());
		modal.add(paragraph);

		Image thumbnail = new Image();

		thumbnail.setUrl(getThumbnailUrl());
		thumbnail.setAltText(getTitle());
		thumbnail.setTitle(getTitle());

		modal.add(thumbnail);
	}

	private void setIndexIndicator(HTMLPanel indexIndicator) {
		indexIndicator.setStylePrimaryName("photo-index-indicator");
	}

	private String buildIndexIndicator() {
		return "#" + getDisplayIndex();
	}

	private void setTitle(HTMLPanel title) {
		title.setStylePrimaryName("photo-title");
		title.add(indexIndicator);
		title.add(new HTMLPanel(SpanElement.TAG, getTitle()));
	}

	private void setPanel(FocusPanel wrapper, FlowPanel panel) {
		wrapper.setStylePrimaryName("photo-container animated " + repository.getPhotosAnimation().in());

		panel.add(image);
		panel.add(title);

		wrapper.add(panel);

		wrapper.addClickHandler(buildClickHandler());
	}

	private void setImage() {
		image.setTitle(getTitle());
		image.setAltText(getTitle());
		image.setUrl(getUrl());
		image.setVisible(true);

		image.setStylePrimaryName("photo-image");
	}

	private ClickHandler buildClickHandler() {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				modal.center();
				modal.show();
			}
		};
	}

	public int getIndex() {
		return index;
	}

	public int getDisplayIndex() {
		return getIndex() + 1;
	}

	public final int getAlbumId() {
		return photo.getAlbumId();
	}

	public final int getId() {
		return photo.getId();
	}

	public final String getTitle() {
		return photo.getTitle();
	}

	public final String getUrl() {
		return photo.getUrl();
	}

	public final String getThumbnailUrl() {
		return photo.getThumbnailUrl();
	}
}