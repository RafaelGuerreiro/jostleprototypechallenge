package com.jostle.prototype.challenge.client.view.widget;

import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ModalWidget extends PopupPanel {

	private final SimplePanel dialog = new SimplePanel();
	private final FlowPanel content = new FlowPanel();
	private final FlowPanel header = new FlowPanel();
	private final FlowPanel body = new FlowPanel();
	private final FlowPanel footer = new FlowPanel();

	public ModalWidget(String title) {
		initialize(title);

		content.add(header);
		content.add(body);
		content.add(footer);

		dialog.add(content);

		super.add(dialog);
	}

	private void initialize(String title) {
		setModal(true);

		setStylePrimaryName("modal modal-widget");

		dialog.setStylePrimaryName("modal-dialog");
		content.setStylePrimaryName("modal-content");
		header.setStylePrimaryName("modal-header");
		body.setStylePrimaryName("modal-body");
		footer.setStylePrimaryName("modal-footer");

		initializeHeader(title);
		initializeFooter();
	}

	private void initializeHeader(String title) {
		Button close = buildCloseButton("<span aria-hidden=\"true\">&times;</span>", "close");
		header.add(close);

		HTMLPanel heading = new HTMLPanel(HeadingElement.TAG_H4, title);
		heading.setStylePrimaryName("modal-title");

		header.add(heading);
	}

	private void initializeFooter() {
		Button close = buildCloseButton("Close", "btn btn-block btn-default");
		footer.add(close);
	}

	private Button buildCloseButton(String content, String classes) {
		Button close = new Button(content);
		close.setStylePrimaryName(classes);
		close.getElement().setAttribute("data-dismiss", "modal");
		close.getElement().setAttribute("type", "button");

		close.addClickHandler(buildCloseModalOnClick());

		return close;
	}

	private ClickHandler buildCloseModalOnClick() {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();
			}
		};
	}

	@Override
	public void add(Widget w) {
		body.add(w);
	}

	@Override
	public void add(IsWidget w) {
		body.add(w);
	}

	public void addToBody(Widget w) {
		add(w);
	}

	public void addToBody(IsWidget w) {
		add(w);
	}

	public void addToHeader(Widget w) {
		header.add(w);
	}

	public void addToHeader(IsWidget w) {
		header.add(w);
	}

	public void addToFooter(Widget w) {
		footer.add(w);
	}

	public void addToFooter(IsWidget w) {
		footer.add(w);
	}
}
