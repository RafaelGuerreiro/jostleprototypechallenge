package com.jostle.prototype.challenge.client.view.widget;

import java.util.LinkedList;
import java.util.List;

import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.jostle.prototype.challenge.client.view.validator.Validator;

public class ValidableTextBox extends TextBox {
	private static final String ERROR_CLASS = "error-danger";

	private final List<Validator> validators = new LinkedList<>();

	private boolean areValidatorHandlersAttached = false;
	private HTMLPanel errors;

	public ValidableTextBox setAttribute(String attribute, Object value) {
		if (attribute != null && value != null)
			getElement().setAttribute(attribute, value.toString());

		return this;
	}

	public ValidableTextBox addValidator(Validator validator) {
		if (validator != null) {
			validators.add(validator);
			addValidableHandlers();
		}

		return this;
	}

	public boolean validate() {
		removeStyleName(ERROR_CLASS);

		List<String> invalids = getInvalids();
		if (invalids.isEmpty())
			return true;

		addStyleName(ERROR_CLASS);

		addErrorsSpan();

		insertMessagesIntoErrorsSpan(invalids);

		return false;
	}

	private void addErrorsSpan() {
		if (errors != null)
			return;

		errors = new HTMLPanel(SpanElement.TAG, "");
		errors.setStylePrimaryName("errors-list");

		getElement().getParentElement().insertAfter(errors.getElement(), getElement());
	}

	private void insertMessagesIntoErrorsSpan(List<String> invalids) {
		StringBuilder messages = new StringBuilder();

		String lineBreak = "";
		for (String message : invalids) {
			messages.append(lineBreak).append(message);
			lineBreak = "<br />";
		}

		errors.getElement().setInnerText(messages.toString());
	}

	private List<String> getInvalids() {
		List<String> invalids = new LinkedList<>();

		if (validators.isEmpty())
			return invalids;

		String value = getValue();
		for (Validator validator : validators)
			if (!validator.validate(value))
				invalids.add(validator.getMessage());

		return invalids;
	}

	protected ValidableTextBox addValidableHandlers() {
		if (areValidatorHandlersAttached)
			return this;

		addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				validate();
			}
		});

		addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				validate();
			}
		});

		areValidatorHandlersAttached = true;

		return this;
	}
}