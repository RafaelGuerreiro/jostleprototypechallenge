package com.jostle.prototype.challenge.client.view.widget;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.ListBox;

public class SelectableListBox extends ListBox {

	private final Map<String, Integer> values = new HashMap<>();

	@Override
	public void addItem(String item, String value) {
		if (value == null)
			throw new IllegalArgumentException("The value cannot be null.");

		if (values.containsKey(value))
			throw new IllegalArgumentException("The value [" + value + "] was already added to this list.");

		super.addItem(item, value);
		values.put(value, getItemCount() - 1);
	}

	public void setSelectedValue(String value) {
		Integer index = values.get(value);
		if (index == null)
			return;

		setSelectedIndex(index);
	}
}
