package com.jostle.prototype.challenge.client.view.pagination;

public class FixedRatePageCalculator implements PageCalculator {

	private int shift;

	public FixedRatePageCalculator(int shift) {
		this.shift = shift;
	}

	@Override
	public int getNextPage(int actualPage) {
		if (actualPage == 0 && shift < 0)
			return actualPage;

		return actualPage + shift;
	}
}