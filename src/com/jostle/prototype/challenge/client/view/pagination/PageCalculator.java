package com.jostle.prototype.challenge.client.view.pagination;

public interface PageCalculator {
	int getNextPage(int actualPage);
}
