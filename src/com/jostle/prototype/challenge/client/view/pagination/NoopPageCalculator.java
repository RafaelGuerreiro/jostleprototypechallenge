package com.jostle.prototype.challenge.client.view.pagination;

public class NoopPageCalculator implements PageCalculator {
	@Override
	public int getNextPage(int actualPage) {
		return actualPage;
	}
}
