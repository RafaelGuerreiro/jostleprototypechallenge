package com.jostle.prototype.challenge.client.view.pagination;

import static java.lang.Integer.parseInt;

public class FixedPageCalculator implements PageCalculator {

	private int page;

	public FixedPageCalculator(int page) {
		this.page = page;
	}

	public FixedPageCalculator(String page) {
		this(parseInt(page));
	}

	@Override
	public int getNextPage(int actualPage) {
		if (page < 0)
			return actualPage;

		return page;
	}
}