package com.jostle.prototype.challenge.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsonUtils;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.dom.client.SourceElement;
import com.google.gwt.dom.client.VideoElement;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.jostle.prototype.challenge.client.VideoService;
import com.jostle.prototype.challenge.client.VideoServiceAsync;
import com.jostle.prototype.challenge.client.json.VideoJavaScriptObject;
import com.jostle.prototype.challenge.client.repository.ClientRepository;

public class VideoView extends CompositeView {
	private static VideoViewUiBinder uiBinder = GWT.create(VideoViewUiBinder.class);

	interface VideoViewUiBinder extends UiBinder<Widget, VideoView> {
	}

	private final VideoServiceAsync service = GWT.create(VideoService.class);

	private final ClientRepository repository;

	@UiField
	HeadingElement title;

	@UiField
	ParagraphElement body;

	@UiField
	VideoElement video;

	public VideoView(ClientRepository repository) {
		initWidget(uiBinder.createAndBindUi(this));
		this.repository = repository;
	}

	@Override
	public void onDisplay() {
		updateVideosSouce();
	}

	private void updateVideosSouce() {
		video.removeAllChildren();

		Element source = new HTMLPanel(SourceElement.TAG, "").getElement();
		source.setAttribute("src", repository.getVideoURL());
		source.setAttribute("type", "video/mp4");

		video.appendChild(source);

		video.load();
		video.play();
	}

	@Override
	public String getPageTitle() {
		return repository.getVideoViewTitle();
	}

	@Override
	protected void loadResource() {
		service.loadResource(repository.getVideoResourceURL(), getServiceCallback());
	}

	private AsyncCallback<String> getServiceCallback() {
		return new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				setVideo(JsonUtils.<VideoJavaScriptObject> safeEval(result));
			}

			@Override
			public void onFailure(Throwable caught) {
				throw new RuntimeException(caught);
			}
		};
	}

	public void setVideo(VideoJavaScriptObject obj) {
		title.setInnerText(obj.getTitle());
		body.setInnerText(obj.getBody());
	}
}
