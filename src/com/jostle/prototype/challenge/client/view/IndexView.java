package com.jostle.prototype.challenge.client.view;

import static com.google.gwt.core.client.GWT.create;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.Widget;
import com.jostle.prototype.challenge.client.repository.ClientRepository;
import com.jostle.prototype.challenge.client.repository.implementation.DefaultClientRepository;

public class IndexView extends Composite {
	private static IndexViewUiBinder uiBinder = GWT.create(IndexViewUiBinder.class);

	interface IndexViewUiBinder extends UiBinder<Widget, IndexView> {
	}

	private final ClientRepository repository = new DefaultClientRepository();

	private static final String TITLE = "Jostle Prototype Challenge";

	public static String buildTitleFor(String title) {
		return title + " - " + TITLE;
	}

	private static int anchorsSize;

	@UiField
	DeckPanel panel;

	@UiField
	Anchor videoView;

	@UiField
	Anchor photosView;

	@UiField
	Anchor creativeView;

	private AnchorView activeView;

	@SuppressWarnings("serial")
	public IndexView() {
		initWidget(uiBinder.createAndBindUi(this));

		setupAnchors(new ArrayList<AnchorView>(3) {
			{
				int index = 0;

				add(new AnchorView(repository, panel, videoView, new VideoView(repository), index++));
				add(new AnchorView(repository, panel, photosView, new PhotosView(repository), index++));
				add(new AnchorView(repository, panel, creativeView, new CreativeView(repository), index++));

				anchorsSize = 12 / size();
			}
		});

		videoView.fireEvent((ClickEvent) create(ClickEvent.class));
	}

	private void setupAnchors(final List<AnchorView> anchorViews) {
		for (AnchorView anchorView : anchorViews)
			anchorView.initializeWithEvent(buildClickHandlerFor(anchorView));
	}

	private ClickHandler buildClickHandlerFor(final AnchorView anchorView) {
		return new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				showWidget(anchorView);
			}
		};
	}

	private void showWidget(final AnchorView targetView) {
		if (activeView == targetView)
			return;

		int waitTime = 0;

		if (activeView != null) {
			activeView.animatedOut();
			waitTime = 500;
		}

		fadeInTargetWidget(targetView, waitTime);
	}

	private void fadeInTargetWidget(final AnchorView targetView, int waitTime) {
		new Timer() {
			@Override
			public void run() {
				setDocumentTitle(targetView.getPageTitle());
				targetView.animatedIn();
				panel.showWidget(targetView.getIndex());

				activeView = targetView;
			}
		}.schedule(waitTime);
	}

	private void setDocumentTitle(String title) {
		Document.get().setTitle(buildTitleFor(title));
	}

	private static class AnchorView {
		private final ClientRepository repository;
		private final DeckPanel panel;
		private final Anchor anchor;
		private final View view;
		private final int index;

		public AnchorView(ClientRepository repository, DeckPanel panel, Anchor anchor, View view, int index) {
			this.repository = repository;
			this.panel = panel;
			this.anchor = anchor;
			this.view = view;
			this.index = index;
		}

		public void initializeWithEvent(ClickHandler clickHandler) {
			getWidget().setStyleName(getViewClasses(repository.getTransitionAnimation().in()));
			getWidget().setWidth("80%");

			panel.add(getWidget());

			getAnchor().setStyleName(getAnchorClasses(""));
			getAnchor().addClickHandler(clickHandler);
		}

		public void animatedIn() {
			getWidget().setStyleName(getViewClasses(repository.getTransitionAnimation().in()));
			getAnchor().setStyleName(getAnchorClasses("active"));

			getView().loadBackendData();
			getView().onDisplay();
		}

		public void animatedOut() {
			getWidget().setStyleName(getViewClasses(repository.getTransitionAnimation().out()));

			getAnchor().setStyleName(getAnchorClasses());
		}

		public String getPageTitle() {
			return getView().getPageTitle();
		}

		public Widget getWidget() {
			return (Widget) view;
		}

		public Anchor getAnchor() {
			return anchor;
		}

		public View getView() {
			return view;
		}

		public int getIndex() {
			return index;
		}

		private String getViewClasses(String additionalClasses) {
			return "animated center-block " + additionalClasses;
		}

		private String getAnchorClasses() {
			return getAnchorClasses("");
		}

		private String getAnchorClasses(String additionalClasses) {
			return "view-selection col-xs-" + anchorsSize + " " + additionalClasses;
		}
	}
}
