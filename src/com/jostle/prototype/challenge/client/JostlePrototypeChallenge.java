package com.jostle.prototype.challenge.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.jostle.prototype.challenge.client.view.IndexView;

public class JostlePrototypeChallenge implements EntryPoint {
	public void onModuleLoad() {
		RootPanel.get("main-container").add(new IndexView());
	}
}