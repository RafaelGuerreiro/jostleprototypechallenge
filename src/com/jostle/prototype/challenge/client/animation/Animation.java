package com.jostle.prototype.challenge.client.animation;

public enum Animation {
	// @formatter:off
	FADE, BOUNCE, LIGHT_SPEED, ROTATE, ROLL, ZOOM,
	FLIP_HORIZONTALLY("flipInX", "flipOutX"),
	FLIP_VERTICALLY("flipInY", "flipOutY"),
	SLIDE_UP("slideInUp", "slideOutUp"),
	SLIDE_RIGHT("slideInRight", "slideOutRight"),
	SLIDE_DOWN("slideInDown", "slideOutDown"),
	SLIDE_LEFT("slideInLeft", "slideOutLeft");
	// @formatter:on

	private final String in;
	private final String out;

	private Animation() {
		this(null, null);
	}

	private Animation(String in, String out) {
		this.in = in == null ? camelCasedName() + "In" : in;
		this.out = out == null ? camelCasedName() + "Out" : out;
	}

	private String camelCasedName() {
		String[] names = name().split("_");

		StringBuilder camelCasedName = new StringBuilder(names[0].toLowerCase());

		if (names.length == 1)
			return camelCasedName.toString();

		for (int index = 1; index < names.length; index++)
			camelCasedName.append(capitalize(names[index].trim()));

		return camelCasedName.toString();
	}

	private String capitalize(String name) {
		if (name.length() == 0)
			return "";

		StringBuilder capitalized = new StringBuilder(String.valueOf(name.charAt(0)));

		if (name.length() > 1)
			capitalized.append(name.substring(1).toLowerCase());

		return capitalized.toString();
	}

	public String getDisplay() {
		String[] names = name().split("_");

		StringBuilder display = new StringBuilder();

		String space = "";
		for (String name : names) {
			display.append(space).append(capitalize(name));
			space = " ";
		}

		return display.toString();
	}

	public String in() {
		return in;
	}

	public String out() {
		return out;
	}
}
