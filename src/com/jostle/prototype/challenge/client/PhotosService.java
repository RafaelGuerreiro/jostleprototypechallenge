package com.jostle.prototype.challenge.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("photos")
public interface PhotosService extends RemoteService {
	String loadResource(String url);
}
