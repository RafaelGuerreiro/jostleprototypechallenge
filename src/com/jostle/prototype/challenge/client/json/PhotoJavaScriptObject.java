package com.jostle.prototype.challenge.client.json;

import com.google.gwt.core.client.JavaScriptObject;

public class PhotoJavaScriptObject extends JavaScriptObject {

	protected PhotoJavaScriptObject() {
	}

	public final native int getAlbumId() /*-{
		return this.albumId;
	}-*/;

	public final native int getId() /*-{
		return this.id;
	}-*/;

	public final native String getTitle() /*-{
		return this.title;
	}-*/;

	public final native String getUrl() /*-{
		return this.url;
	}-*/;

	public final native String getThumbnailUrl() /*-{
		return this.thumbnailUrl;
	}-*/;
}
