package com.jostle.prototype.challenge.client.json;

import com.google.gwt.core.client.JavaScriptObject;

public class VideoJavaScriptObject extends JavaScriptObject {

	protected VideoJavaScriptObject() {
	}

	public final native int getUserId() /*-{
		return this.userId;
	}-*/;

	public final native int getId() /*-{
		return this.id;
	}-*/;

	public final native String getTitle() /*-{
		return this.title;
	}-*/;

	public final native String getBody() /*-{
		return this.body;
	}-*/;
}
