package com.jostle.prototype.challenge.client.repository;

import com.jostle.prototype.challenge.client.animation.Animation;
import com.jostle.prototype.challenge.client.converter.Converter;
import com.jostle.prototype.challenge.client.converter.implementation.AnimationConverter;
import com.jostle.prototype.challenge.client.converter.implementation.IntegerConverter;

public enum RepositoryKey {
	// @formatter:off
	TRANSITION_ANIMATION(Animation.FADE, new AnimationConverter()),
	VIDEO_VIEW_TITLE("Video"),
	VIDEO_URL("http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4"),
	VIDEO_RESOURCE_URL("http://jsonplaceholder.typicode.com/posts/1"),
	PHOTOS_VIEW_TITLE("Photos"),
	PHOTOS_URL("http://jsonplaceholder.typicode.com/photos"),
	PHOTOS_PER_PAGE(10, new IntegerConverter()),
	PHOTOS_ANIMATION(Animation.FADE, new AnimationConverter()),
	CREATIVE_VIEW_TITLE("Creative");
	// @formatter:on

	private final Object defaultValue;
	private final Converter<?> converter;

	private RepositoryKey(Object defaultValue) {
		this(defaultValue, null);
	}

	private RepositoryKey(Object defaultValue, Converter<?> converter) {
		this.defaultValue = defaultValue;
		this.converter = converter;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public Object convert(String value) {
		if (converter == null)
			return value;

		return converter.convert(value);
	}
}