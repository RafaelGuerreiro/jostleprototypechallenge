package com.jostle.prototype.challenge.client.repository.implementation;

import java.util.HashMap;
import java.util.Map;

import com.jostle.prototype.challenge.client.animation.Animation;
import com.jostle.prototype.challenge.client.repository.ClientRepository;
import com.jostle.prototype.challenge.client.repository.RepositoryKey;

public class DefaultClientRepository implements ClientRepository {

	private final Map<RepositoryKey, Object> data = new HashMap<>();

	public DefaultClientRepository() {
		for (RepositoryKey repositoryKey : RepositoryKey.values())
			data.put(repositoryKey, repositoryKey.getDefaultValue());
	}

	@Override
	public Animation getTransitionAnimation() {
		return get(RepositoryKey.TRANSITION_ANIMATION);
	}

	@Override
	public void setTransitionAnimation(String animation) {
		set(RepositoryKey.TRANSITION_ANIMATION, animation);
	}

	@Override
	public String getVideoViewTitle() {
		return get(RepositoryKey.VIDEO_VIEW_TITLE);
	}

	@Override
	public void setVideoViewTitle(String title) {
		set(RepositoryKey.VIDEO_VIEW_TITLE, title);
	}

	@Override
	public String getVideoURL() {
		return get(RepositoryKey.VIDEO_URL);
	}

	@Override
	public void setVideoURL(String url) {
		set(RepositoryKey.VIDEO_URL, url);
	}

	@Override
	public String getVideoResourceURL() {
		return get(RepositoryKey.VIDEO_RESOURCE_URL);
	}

	@Override
	public void setVideoResourceURL(String url) {
		set(RepositoryKey.VIDEO_RESOURCE_URL, url);
	}

	@Override
	public String getPhotosViewTitle() {
		return get(RepositoryKey.PHOTOS_VIEW_TITLE);
	}

	@Override
	public void setPhotosViewTitle(String title) {
		set(RepositoryKey.PHOTOS_VIEW_TITLE, title);
	}

	@Override
	public int getPhotosPerPage() {
		return get(RepositoryKey.PHOTOS_PER_PAGE);
	}

	@Override
	public void setPhotosPerPage(String limit) {
		set(RepositoryKey.PHOTOS_PER_PAGE, limit);
	}

	@Override
	public String getPhotosURL() {
		return get(RepositoryKey.PHOTOS_URL);
	}

	@Override
	public void setPhotosURL(String url) {
		set(RepositoryKey.PHOTOS_URL, url);
	}

	@Override
	public Animation getPhotosAnimation() {
		return get(RepositoryKey.PHOTOS_ANIMATION);
	}

	@Override
	public void setPhotosAnimation(String animation) {
		set(RepositoryKey.PHOTOS_ANIMATION, animation);
	}

	@Override
	public String getCreativeViewTitle() {
		return get(RepositoryKey.CREATIVE_VIEW_TITLE);
	}

	@Override
	public void setCreativeViewTitle(String title) {
		set(RepositoryKey.CREATIVE_VIEW_TITLE, title);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(RepositoryKey key) {
		if (key == null)
			throw new IllegalArgumentException("The key cannot be null.");

		return (T) data.get(key);
	}

	@Override
	public void set(RepositoryKey key, String value) {
		if (key == null)
			throw new IllegalArgumentException("The key cannot be null.");

		if (value == null)
			throw new IllegalArgumentException("The value cannot be null.");

		data.put(key, key.convert(value));
	}
}