package com.jostle.prototype.challenge.client.repository;

import com.jostle.prototype.challenge.client.animation.Animation;

public interface ClientRepository {
	Animation getTransitionAnimation();

	void setTransitionAnimation(String animation);

	String getVideoViewTitle();

	void setVideoViewTitle(String title);

	String getVideoURL();

	void setVideoURL(String url);

	String getVideoResourceURL();

	void setVideoResourceURL(String url);

	String getPhotosViewTitle();

	void setPhotosViewTitle(String title);

	int getPhotosPerPage();

	void setPhotosPerPage(String limit);

	String getPhotosURL();

	void setPhotosURL(String url);

	Animation getPhotosAnimation();

	void setPhotosAnimation(String animation);

	String getCreativeViewTitle();

	void setCreativeViewTitle(String title);

	void set(RepositoryKey repositoryKey, String value);

	<T> T get(RepositoryKey repositoryKey);
}