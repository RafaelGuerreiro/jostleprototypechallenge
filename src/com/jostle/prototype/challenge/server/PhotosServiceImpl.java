package com.jostle.prototype.challenge.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jostle.prototype.challenge.client.PhotosService;

public class PhotosServiceImpl extends RemoteServiceServlet implements PhotosService {
	private static final long serialVersionUID = 2672340001618112010L;

	private final RemoteResouceLoader delegate;

	public PhotosServiceImpl() {
		delegate = new RemoteResouceLoader("photos.json");
	}

	@Override
	public String loadResource(String url) {
		return delegate.loadResource(url);
	}
}