package com.jostle.prototype.challenge.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jostle.prototype.challenge.client.VideoService;

public class VideoServiceImpl extends RemoteServiceServlet implements VideoService {
	private static final long serialVersionUID = 2672340001618112010L;

	private final RemoteResouceLoader delegate;

	public VideoServiceImpl() {
		delegate = new RemoteResouceLoader("video.json");
	}

	@Override
	public String loadResource(String url) {
		return delegate.loadResource(url);
	}
}