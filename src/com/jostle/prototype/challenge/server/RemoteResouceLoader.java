package com.jostle.prototype.challenge.server;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class RemoteResouceLoader {

	private final String localFile;

	public RemoteResouceLoader() {
		this(null);
	}

	public RemoteResouceLoader(String localFile) {
		this.localFile = localFile;
	}

	public String loadResource(String url) {
		try {
			URL uri = new URL(url);
			InputStream content = (InputStream) uri.getContent();

			if (content == null)
				throw new IllegalStateException("The response is null.");

			return fromInputStreamToString(content);
		} catch (IOException e) {
			try {
				if (localFile != null)
					return tryTheLocalFile();
			} catch (Exception exception) {
				// shadowed
			}

			throw new RuntimeException(e);
		}
	}

	private String tryTheLocalFile() throws IOException {
		InputStream stream = getClass().getClassLoader().getResourceAsStream(localFile);
		return fromInputStreamToString(stream);
	}

	private String fromInputStreamToString(InputStream content) throws IOException {
		int byteAmount = content.available();

		byte[] bytes = new byte[byteAmount];
		content.read(bytes, 0, byteAmount);

		return new String(bytes, UTF_8);
	}
}